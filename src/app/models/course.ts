import { Department } from "./department";
import { User } from "./user";

export interface Course {
    id: number;
    departmentId: Department;
    name: string;
    slug: string;
    image: string;
    fees: number;
    shifts: string;
    userId: User;
    status: boolean;
}
