import { Course } from "./course";
import { Department } from "./department";

export interface Application {
    id: number;
    fullName: string;
    departmentId: Department;
    courseId: Course;
    shift: string;
    entryDate: Date;
    status: boolean;
}
