import { User } from "./user";

export interface Department {
    id: number;
    name: string;
    slug: string;
    userId: User;
    status: boolean;
}
