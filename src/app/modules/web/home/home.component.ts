import { Component, OnInit } from '@angular/core';
import { Department } from 'src/app/models/department';
import { ToastService } from '../../common/toast/toast.service';
import { WebService } from '../web.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  departments: Department[] = [];

  constructor(
    public webService: WebService,
    public toastService: ToastService
    ) { }

  ngOnInit(): void {
    this.webService.departmentList().subscribe((data: Department[]) => {
      this.departments = data;
    })
  }

  showStandard() {
    this.toastService.show('hello', {
      classname: 'bg-white text-dark',
      delay: 2000,
      autohide: true,
      // headertext: 'Here goes the message'
    });
  }

  showSuccess() {
    this.toastService.show('I am a success toast', {
      classname: 'bg-success text-light',
      delay: 2000 ,
      autohide: false,
      headertext: 'Toast Header'
    });
  }
  showError() {
    this.toastService.show('I am a success toast', {
      classname: 'bg-danger text-light',
      delay: 2000 ,
      autohide: true,
      headertext: 'Error!!!'
    });
  }

  showCustomToast(customTpl) {
    this.toastService.show(customTpl, {
      classname: 'bg-info text-light',
      delay: 3000,
      autohide: true
    });
  }

}
