import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  errors: any[] = [];

  constructor(
    public auth: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    this.auth.login(this.form.value).subscribe(data => {
      if(data['code'] == 200){
        this.auth.setToken(data['access_token']);
        this.router.navigate([''])
        .then(()=>{
          window.location.reload();
        })
      }else{
        Object.entries(data).forEach(([key, value], index) => {
          this.errors.push(value[0]);
        });
      }
    });
  }

}
