import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { appInfo } from 'src/app/config/appInfo.const';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-web',
  templateUrl: './web.component.html',
  styleUrls: ['./web.component.css']
})
export class WebComponent implements OnInit {

  appName = appInfo.appName;
  authCheck: any;
  user: User;

  constructor(
    public auth: AuthService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.authCheck = this.auth.authCheck();
    if(this.authCheck){
      this.auth.user().subscribe(data => {
        this.user = data;
      });
    }
  }

  homeLink() {
    return this.router.navigate(['']);
  }

  aboutUsLink() {
    return this.router.navigate(['about-us']);
  }

  contactLink() {
    return this.router.navigate(['contact']);
  }

  registerLink() {
    return this.router.navigate(['register']);
  }

  loginLink() {
    return this.router.navigate(['login']);
  }

  profileLink() {
    return this.router.navigate(['profile']);
  }

  dashboardLink() {
    return this.router.navigate(['dashboard']);
  }

  logout() {
    this.auth.logout();
    this.router.navigate([''])
    .then(()=> {
      window.location.reload();
    })
  }

}
