import { Injectable } from '@angular/core';
import { appInfo } from 'src/app/config/appInfo.const';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WebService {

  baseUrl = appInfo.baseUrl;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    })
  }

  constructor(private httpClient: HttpClient) { }

  departmentList(): Observable<any> {
    return this.httpClient.get<any>(this.baseUrl + '/department-list')
    .pipe(
      catchError(this.errorHandler)
    )
  }

  departmentDetails(slug: string): Observable<any> {
    return this.httpClient.get<any>(this.baseUrl + '/department-details/' + slug)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent)
    {
      errorMessage = error.error.message;
    }else{
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
