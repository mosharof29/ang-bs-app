import { Component, OnInit } from '@angular/core';
import { WebService } from '../web.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HelperService } from 'src/app/services/helper.service';
import { Department } from 'src/app/models/department';
import { Course } from 'src/app/models/course';

@Component({
  selector: 'app-department-details',
  templateUrl: './department-details.component.html',
  styleUrls: ['./department-details.component.css']
})
export class DepartmentDetailsComponent implements OnInit {
  
  slug: string;
  department: Department;
  courses: Course[] = [];
  imagePath: string;

  constructor(
    public webService: WebService,
    public helper: HelperService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.slug = this.route.snapshot.params['slug'];
    this.webService.departmentDetails(this.slug).subscribe(data => {
      this.department = data;
      this.courses = data['courses'];
    });
    this.helper.getImagePath().subscribe(data => {
      this.imagePath = data;
    });
  }

}
