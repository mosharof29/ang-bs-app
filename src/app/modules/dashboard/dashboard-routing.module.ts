import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationInputsComponent } from './application/application-inputs/application-inputs.component';
import { ApplicationListComponent } from './application/application-list/application-list.component';
import { CourseInputsComponent } from './course/course-inputs/course-inputs.component';
import { CourseListComponent } from './course/course-list/course-list.component';
import { CourseViewComponent } from './course/course-view/course-view.component';
import { DashboardComponent } from './dashboard.component';
import { DepartmentInputsComponent } from './department/department-inputs/department-inputs.component';
import { DepartmentListComponent } from './department/department-list/department-list.component';
import { DepartmentViewComponent } from './department/department-view/department-view.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {path: '', redirectTo: 'home', pathMatch: 'full'},
      {path: 'home', component: HomeComponent},
      // department
      {path: 'department-list', component: DepartmentListComponent},
      {path: 'department-create', component: DepartmentInputsComponent},
      {path: 'department-view/:id', component: DepartmentViewComponent},
      {path: 'department-edit/:id', component: DepartmentInputsComponent},
      // course
      {path: 'course-list', component: CourseListComponent},
      {path: 'course-create', component: CourseInputsComponent},
      {path: 'course-view/:id', component: CourseViewComponent},
      {path: 'course-edit/:id', component: CourseInputsComponent},
      // application
      {path: 'application-list', component: ApplicationListComponent},
      {path: 'application-create', component: ApplicationInputsComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
