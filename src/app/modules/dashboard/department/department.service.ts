import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { appInfo } from 'src/app/config/appInfo.const';
import { Department } from 'src/app/models/department';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Controll-Allow-Origin': '*'
    })
  }

  constructor(
    public httpClient: HttpClient,
    private router: Router
  ) { }

  departments(): Observable<Department[]> {
    return this.httpClient.get<Department[]>(appInfo.baseUrl + '/departments', this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  create(department: any): Observable<Department> {
    return this.httpClient.post<Department>(appInfo.baseUrl + '/departments/store', JSON.stringify(department), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  find(id: number): Observable<Department> {
    return this.httpClient.get<Department>(appInfo.baseUrl + '/departments/show/' + id, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  update(id: number, department: any): Observable<Department> {
    return this.httpClient.put<Department>(appInfo.baseUrl + '/departments/update/' + id, JSON.stringify(department), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  delete(id: number) {
    return this.httpClient.delete<Department>(appInfo.baseUrl + '/departments/delete/' + id, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }
  
  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent)
    {
      errorMessage = error.error.message;
    }else{
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

}
