import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentInputsComponent } from './department-inputs.component';

describe('DepartmentInputsComponent', () => {
  let component: DepartmentInputsComponent;
  let fixture: ComponentFixture<DepartmentInputsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentInputsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentInputsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
