import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Department } from 'src/app/models/department';
import { ToastService } from 'src/app/modules/common/toast/toast.service';
import { DepartmentService } from '../department.service';

@Component({
  selector: 'app-department-inputs',
  templateUrl: './department-inputs.component.html',
  styleUrls: ['./department-inputs.component.css']
})
export class DepartmentInputsComponent implements OnInit {
  
  editMode: boolean = false;
  id!: number;
  department!: Department;
  form!: FormGroup;

  constructor(
    public departmentService: DepartmentService,
    private router: Router,
    private route: ActivatedRoute,
    public toastService: ToastService
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required])
    });
    if(this.route.snapshot.params['id'])
    {
      this.editMode = true;
      this.id = this.route.snapshot.params['id'];
      this.departmentService.find(this.id).subscribe(data => {
        this.department = data;
        this.form.controls.name.setValue(this.department.name);
      });
    }
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    if(this.editMode)
    {
      this.departmentService.update(this.id, this.form.value).subscribe(res => {
        this.router.navigate(['department-list'])
        .then(()=>{
          this.toastService.show('Department has been updated!', {
            classname: 'bg-success text-light',
            delay: 2000 ,
            autohide: true,
            headertext: 'Success!!'
          });
        });
      });
    }else{
      this.departmentService.create(this.form.value).subscribe(res => {
        this.router.navigate(['department-list'])
        .then(()=>{
          this.toastService.show('Department has been created!', {
            classname: 'bg-success text-light',
            delay: 2000 ,
            autohide: true,
            headertext: 'Success!!'
          });
        });
      });
    }
  }

  departmentListLink() {
    return this.router.navigate(['department-list']);;
  }

}
