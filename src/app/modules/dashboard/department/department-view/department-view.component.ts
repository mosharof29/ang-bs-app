import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Course } from 'src/app/models/course';
import { Department } from 'src/app/models/department';
import { HelperService } from 'src/app/services/helper.service';
import { DepartmentService } from '../department.service';

@Component({
  selector: 'app-department-view',
  templateUrl: './department-view.component.html',
  styleUrls: ['./department-view.component.css']
})
export class DepartmentViewComponent implements OnInit {

  id!: number;
  department!: Department;
  courses: Course[] = [];
  imagePath: string;

  constructor(
    public departmentService: DepartmentService,
    private helper: HelperService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.departmentService.find(this.id).subscribe((data: Department) => {
      this.department = data;
      this.courses = data['courses'];
    });
    this.helper.getImagePath().subscribe(data => {
      this.imagePath = data;
    });
  }

  departmentListLink() {
    return this.router.navigate(['department-list']);;
  }

}
