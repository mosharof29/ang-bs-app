import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Department } from 'src/app/models/department';
import { DepartmentService } from '../department.service';

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css']
})
export class DepartmentListComponent implements OnInit {

  departments: Department[] = [];

  constructor(
    public departmentService: DepartmentService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.departmentService.departments().subscribe((data: Department[]) => {
      this.departments = data;
    });
  }

  departmentCreateLink(): void {
    this.router.navigate(['department-create']);
  }

  departmentEditLink(id: number): void {
    this.router.navigate(['department-edit/', id]);
  }

  departmentViewLink(id: number): void {
    this.router.navigate(['department-view/', id]);
  }

  departmentDelete(id: number): void {
    this.departmentService.delete(id).subscribe(res => {
      if(res['code'] == 105){
        console.log(res['status']);
      }else{
        console.log(res['status']);
        this.departments = this.departments.filter(item => item.id !== id);
      }
    });
  }

}
