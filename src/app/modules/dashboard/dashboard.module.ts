import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard.component';
import { DepartmentListComponent } from './department/department-list/department-list.component';
import { DepartmentInputsComponent } from './department/department-inputs/department-inputs.component';
import { DepartmentViewComponent } from './department/department-view/department-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CourseListComponent } from './course/course-list/course-list.component';
import { CourseInputsComponent } from './course/course-inputs/course-inputs.component';
import { CourseViewComponent } from './course/course-view/course-view.component';
import { RouterModule } from '@angular/router';
import { ApplicationListComponent } from './application/application-list/application-list.component';
import { ApplicationInputsComponent } from './application/application-inputs/application-inputs.component';

@NgModule({
  declarations: [HomeComponent, DashboardComponent, DepartmentListComponent, DepartmentInputsComponent, DepartmentViewComponent, CourseListComponent, CourseInputsComponent, CourseViewComponent, ApplicationListComponent, ApplicationInputsComponent],
  imports: [
    CommonModule,
    RouterModule,
    DashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class DashboardModule { }
