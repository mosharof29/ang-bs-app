import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { appInfo } from 'src/app/config/appInfo.const';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  appName = appInfo.appName;
  authCheck: any;
  user: any;
  sidebarOpen: boolean = false;

  constructor(
    public auth: AuthService,
    private router:Router
    ) { }

  ngOnInit(): void {
    this.authCheck = this.auth.authCheck();
    if(this.authCheck){
      this.auth.user().subscribe(data => {
        this.user = data;
      });
    }
  }

  toggleSidebar() {
    if(!this.sidebarOpen)
    {
      this.sidebarOpen = true;
    }else{
      this.sidebarOpen = false;
    }
  }

  closeSidebar() {
    this.sidebarOpen = false;
  }

  dashboardLink() {
    this.closeSidebar();
    this.router.navigate(['dashboard']);
  }

  departmentListLink() {
    this.closeSidebar();
    return this.router.navigate(['department-list']);;
  }

  courseListLink() {
    this.closeSidebar();
    return this.router.navigate(['course-list']);
  }

  applicationListLink() {
    this.closeSidebar();
    return this.router.navigate(['application-list']);
  }

  homeLink() {
    return this.router.navigate(['']);
  }

  profileLink() {
    return this.router.navigate(['profile']);
  }

  logout() {
    this.auth.logout();
  }

}
