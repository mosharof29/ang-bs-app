import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Application } from 'src/app/models/application';
import { ApplicationService } from '../application.service';

@Component({
  selector: 'app-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.css']
})
export class ApplicationListComponent implements OnInit {

  applications: Application[] =[];

  constructor(
    public applicationService: ApplicationService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.applicationService.applications().subscribe((data: Application[]) => {
      this.applications = data;
    });
  }

  applicationCreateLink()
  {
    this.router.navigate(['application-create']);
  }

}
