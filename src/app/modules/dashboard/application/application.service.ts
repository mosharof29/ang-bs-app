import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { appInfo } from 'src/app/config/appInfo.const';
import { Application } from 'src/app/models/application';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  application: Application;

  httpOptions = {
    headers: new HttpHeaders({
      'Access-Controll-Allow-Origin': '*',
      'Content-Type': 'application/json'
    })
  }

  constructor(public httpClient: HttpClient) { }

  applications(): Observable<Application[]> {
    return this.httpClient.get<Application[]>(appInfo.baseUrl + '/applications', this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent)
    {
      errorMessage = error.error.message;
    }else{
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
