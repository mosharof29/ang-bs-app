import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationInputsComponent } from './application-inputs.component';

describe('ApplicationInputsComponent', () => {
  let component: ApplicationInputsComponent;
  let fixture: ComponentFixture<ApplicationInputsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationInputsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationInputsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
