import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Course } from 'src/app/models/course';
import { Department } from 'src/app/models/department';
import { CourseService } from '../../course/course.service';
import { DepartmentService } from '../../department/department.service';
import { ApplicationService } from '../application.service';

@Component({
  selector: 'app-application-inputs',
  templateUrl: './application-inputs.component.html',
  styleUrls: ['./application-inputs.component.css']
})
export class ApplicationInputsComponent implements OnInit {

  editMode: boolean = false;
  departments: Department[] = [];
  courses: Course[] = [];
  form: FormGroup;
  shifts: any[] = [];
  minDate: any = Date();
  maxDate: any;

  constructor(
    public applicationService: ApplicationService,
    public departmentService: DepartmentService,
    public courseService: CourseService,
    public router: Router
  ) { }

  ngOnInit(): void {
    console.log(this.minDate);
    this.form = new FormGroup({
      full_name: new FormControl('', [Validators.required]),
      department_id: new FormControl('', [Validators.required]),
      course_id: new FormControl('', [Validators.required]),
      shift: new FormControl('', [Validators.required]),
      entry_date: new FormControl('', [Validators.required]),
      status: new FormControl(''),
    });
    this.departmentService.departments().subscribe((data: Department[]) => {
      this.departments = data;
    });
  }

  setCoursesByDepartment(e: any) {
    let departmentId = e;
    this.courseService.coursesByDepartment(departmentId).subscribe((data: any) => {
      this.courses = data;
    });
  }

  setShifts(e: any) {
    let courseId = e;
    this.courseService.find(courseId).subscribe((data: any) => {
      this.shifts = data['shifts'].split(',');
      console.log(this.shifts);
    });
  }

  applicationListLink() {
    return;
  }

  get f() {
    return this.form.controls;
  }

  submit() {
    return;
  }

}
