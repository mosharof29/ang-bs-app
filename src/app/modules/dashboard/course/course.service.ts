import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { appInfo } from 'src/app/config/appInfo.const';
import { Course } from 'src/app/models/course';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  course!: Course;

  httpOptions = {
    headers: new HttpHeaders({
      'Access-Controll-Allow-Origin': '*',
      'Content-Type': 'application/json'
    })
  }

  constructor(
    public httpClient: HttpClient
  ) { }

  courses(): Observable<Course[]> {
    return this.httpClient.get<Course[]>(appInfo.baseUrl + '/courses', this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  coursesByDepartment(id: number): Observable<Course[]> {
    return this.httpClient.get<Course[]>(appInfo.baseUrl + '/courses/' + id, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  create(department: any): Observable<Course> {
    return this.httpClient.post<Course>(appInfo.baseUrl + '/courses/store', JSON.stringify(department), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  find(id: number): Observable<Course> {
    return this.httpClient.get<Course>(appInfo.baseUrl + '/courses/show/' + id, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  update(id: number, department: any): Observable<Course> {
    return this.httpClient.put<Course>(appInfo.baseUrl + '/courses/update/' + id, JSON.stringify(department), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  updateWithImage(id: number, formData: any): Observable<any> {
    return this.httpClient.put<any>(appInfo.baseUrl + '/courses/update/' + id, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  delete(id: number) {
    return this.httpClient.delete<Course>(appInfo.baseUrl + '/courses/delete/' + id, this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  shifts(): Observable<any> {
    return this.httpClient.get<any>(appInfo.baseUrl + '/shifts');
  }

  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent)
    {
      errorMessage = error.error.message;
    }else{
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
