import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Course } from 'src/app/models/course';
import { HelperService } from 'src/app/services/helper.service';
import { CourseService } from '../course.service';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  courses: Course[] = [];
  imagePath: string;

  constructor(
    public courseService: CourseService,
    private router: Router,
    private helper: HelperService
  ) { }

  ngOnInit(): void {
    this.courseService.courses().subscribe((data: Course[]) => {
      this.courses = data;
    });
    this.helper.getImagePath().subscribe(data => {
      this.imagePath = data;
    });
  }

  courseCreateLink(): void {
    this.router.navigate(['course-create']);
  }

  courseViewLink(id: number): void {
    this.router.navigate(['course-view', id]);
  }

  courseEditLink(id: number): void {
    this.router.navigate(['course-edit', id]);
  }

  courseDelete(id: number): void {
    this.courseService.delete(id).subscribe(res => {
      console.log('Course has been deleted!');
      this.courses = this.courses.filter(item => item.id !== id);
    });
  }

}
