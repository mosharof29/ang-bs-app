import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { appInfo } from 'src/app/config/appInfo.const';
import { DepartmentService } from '../../department/department.service';
import { CourseService } from '../course.service';

@Component({
  selector: 'app-course-inputs',
  templateUrl: './course-inputs.component.html',
  styleUrls: ['./course-inputs.component.css']
})
export class CourseInputsComponent implements OnInit {

  editMode: boolean = false;
  id!: number;
  form!: FormGroup;
  course!: any;
  departments: any[] = [];
  shifts: any[] = [];
  images = [];

  constructor(
    public httpClient: HttpClient,
    public courseService: CourseService,
    private router: Router,
    private route: ActivatedRoute,
    private departmentService: DepartmentService
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl('',[Validators.required]),
      departmentId: new FormControl('',[Validators.required]),
      fees: new FormControl('',[Validators.required]),
      shifts: new FormArray([],[Validators.required]),
      status: new FormControl('',[Validators.required]),
      images: new FormControl(''),
    });
    this.departmentService.departments().subscribe((data: any[]) => {
      this.departments = data;
    });
    if(this.route.snapshot.params['id'])
    {
      this.editMode = true;
      this.id = this.route.snapshot.params['id'];
      this.courseService.find(this.id).subscribe((data: any) => {
        this.course = data;
        this.form.controls.name.setValue(this.course.name);
        this.form.controls.departmentId.setValue(this.course.department_id);
        this.form.controls.fees.setValue(this.course.fees);
        this.form.controls.status.setValue(this.course.status ? 'true' : 'false');
      });
    }
    this.courseService.shifts().subscribe((data: any[]) => {
      data.forEach(d => {
        if(this.editMode){
          if(this.course?.shifts && this.course.shifts.includes(d.value))
          {
            this.shifts.push({
              name: d.name,
              value: d.value,
              selected: true
            });
          const setShifts: FormArray = this.form.get('shifts') as FormArray;
          setShifts.push(new FormControl(d.value));
          }else{
            this.shifts.push({
              name: d.name,
              value: d.value,
              selected: false
            });
          }
        }else{
          this.shifts.push({
            name: d.name,
            value: d.value,
            selected: false
          });
        }        
      });
    });
  }

  onCheckboxChange(e) {
    const setShifts: FormArray = this.form.get('shifts') as FormArray;
    if (e.target.checked) {
      setShifts.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      setShifts.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          setShifts.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  onFileChange(event) {  
    for (var i = 0; i < event.target.files.length; i++) { 
        this.images.push(event.target.files[i]);
    }
  }

  get f() {
    return this.form.controls;
  }

  submit(){
    const formData = new FormData();

    for (var i = 0; i < this.images.length; i++) { 
      formData.append("images[]", this.images[i]);
    }

    formData.append('name',this.form.value.name);
    formData.append('departmentId',this.form.value.departmentId);
    formData.append('fees',this.form.value.fees);
    formData.append('shifts[]',this.form.value.shifts);
    formData.append('status',this.form.value.status);

    if(this.editMode)
    {
      this.httpClient.post(appInfo.baseUrl + '/courses/update/' + this.id, formData)
      .subscribe(res => {
        console.log(res);
        // alert('Uploaded Successfully.');
        this.router.navigate(['course-list']);
      })
    }else{
      this.httpClient.post(appInfo.baseUrl + '/courses/store', formData)
      .subscribe(res => {
        console.log(res);
        // alert('Uploaded Successfully.');
        this.router.navigate(['course-list']);
      })
    }
    
  }

  courseListLink() {
    return this.router.navigate(['course-list']);
  }

}
