import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseInputsComponent } from './course-inputs.component';

describe('CourseInputsComponent', () => {
  let component: CourseInputsComponent;
  let fixture: ComponentFixture<CourseInputsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseInputsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseInputsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
