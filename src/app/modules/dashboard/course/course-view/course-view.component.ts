import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Course } from 'src/app/models/course';
import { Department } from 'src/app/models/department';
import { DepartmentService } from '../../department/department.service';
import { CourseService } from '../course.service';

@Component({
  selector: 'app-course-view',
  templateUrl: './course-view.component.html',
  styleUrls: ['./course-view.component.css']
})
export class CourseViewComponent implements OnInit {

  id!: number;
  course!: Course;
  department!: any;
  images!: [];

  constructor(
    public courseService: CourseService,
    public departmentService: DepartmentService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.courseService.find(this.id).subscribe((data: Course) => {
      this.course = data;
      this.images = data['images'];
      this.department = data['department_id'];
      this.departmentService.find(this.department).subscribe((data: Department) => {
        this.department = data;
      });
    })
  }

  courseListLink(): void {
    this.router.navigate(['course-list']);
  }

}
