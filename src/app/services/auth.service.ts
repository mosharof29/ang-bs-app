import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { appInfo } from '../config/appInfo.const';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Acces-Control-Allow-Origin': '*',
      'Authorization': `Bearer ${this.getToken()}`
    })
  }

  constructor(
    public httpClient: HttpClient,
    private router: Router
  ) { }

  setToken(token: string): void {
    localStorage.setItem('bs-app-token', token);
  }
  
  getToken(): string {
    return localStorage.getItem('bs-app-token');
  }

  authCheck() {
    return this.getToken() !== null;
  }

  login(loginData: any): Observable<any> {
    return this.httpClient.post<any>(appInfo.login, JSON.stringify(loginData), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  register(registerData: any): Observable<any> {
    return this.httpClient.post<any>(appInfo.register, JSON.stringify(registerData), this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  user() {
    return this.httpClient.get<User>(appInfo.baseUrl + '/user', this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  logout() :void {
    this.httpClient.post<any>(appInfo.logout, JSON.stringify(this.getToken()), this.httpOptions);
    localStorage.removeItem('bs-app-token');
    this.router.navigate(['']);
  }

  errorHandler(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent)
    {
      errorMessage = error.error.message;
    }else{
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
