import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { appInfo } from '../config/appInfo.const';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  baseUrl = appInfo.baseUrl;

  constructor(public httpClient: HttpClient) { }

  getImagePath(): Observable<any> {
    return this.httpClient.get<any>(appInfo.baseUrl + '/get-image-path');
  }
}
