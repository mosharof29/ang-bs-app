export const appInfo = {
    appName: "ANG-BS-APP",
    baseUrl: "http://127.0.0.1:8090/api",
    register: "http://127.0.0.1:8090/api/register",
    login: "http://127.0.0.1:8090/api/login",
    logout: "http://127.0.0.1:8090/api/logout",
};